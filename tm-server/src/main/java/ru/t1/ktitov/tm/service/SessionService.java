package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.api.repository.ISessionRepository;
import ru.t1.ktitov.tm.api.service.ISessionService;
import ru.t1.ktitov.tm.model.Session;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository>
        implements ISessionService {

    public SessionService(@NotNull final ISessionRepository repository) {
        super(repository);
    }

}
