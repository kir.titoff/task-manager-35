package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.ktitov.tm.api.repository.ITaskRepository;
import ru.t1.ktitov.tm.api.service.ITaskService;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.repository.TaskRepository;

import static ru.t1.ktitov.tm.constant.TaskTestData.*;
import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final ITaskService service = new TaskService(repository);

    @After
    public void tearDown() {
        service.clear();
    }

    @Test
    public void add() {
        service.add(USER1_TASK1);
        service.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1, service.findAll().get(0));
        Assert.assertEquals(USER2_TASK1, service.findAll().get(1));
    }

    @Test
    public void addByUserId() {
        service.add(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, service.findAll().get(0));
        Assert.assertEquals(USER1.getId(), service.findAll().get(0).getUserId());
    }

    @Test
    public void addList() {
        service.add(USER1_TASK_LIST);
        Assert.assertEquals(3, service.getSize());
        Assert.assertEquals(USER1_TASK1, service.findAll().get(0));
        Assert.assertEquals(USER1_TASK2, service.findAll().get(1));
        Assert.assertEquals(USER1_TASK3, service.findAll().get(2));
    }

    @Test
    public void setList() {
        service.add(USER1_TASK_LIST);
        service.set(USER2_TASK_LIST);
        Assert.assertEquals(USER2_TASK1, service.findAll().get(0));
    }

    @Test
    public void clear() {
        service.add(USER1_TASK_LIST);
        Assert.assertEquals(3, service.getSize());
        service.clear();
        Assert.assertEquals(0, service.getSize());
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        service.clear(USER1.getId());
        Assert.assertEquals(1, service.getSize());
        Assert.assertEquals(USER2_TASK1, service.findAll().get(0));
    }

    @Test
    public void findAllByUserId() {
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        service.add(ADMIN1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST, service.findAll(USER1.getId()));
        Assert.assertEquals(USER2_TASK_LIST, service.findAll(USER2.getId()));
        Assert.assertEquals(ADMIN1_TASK_LIST, service.findAll(ADMIN1.getId()));
    }

    @Test
    public void existsById() {
        service.add(USER1_TASK1);
        service.add(USER2_TASK1);
        Assert.assertTrue(service.existsById(USER1_TASK1.getId()));
        Assert.assertTrue(service.existsById(USER2_TASK1.getId()));
        Assert.assertFalse(service.existsById(USER1_TASK2.getId()));
        Assert.assertTrue(service.existsById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertFalse(service.existsById(USER1.getId(), USER2_TASK1.getId()));
    }

    @Test
    public void findOneById() {
        service.add(USER1_TASK1);
        service.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1, service.findOneById(USER1_TASK1.getId()));
        Assert.assertEquals(USER2_TASK1, service.findOneById(USER2_TASK1.getId()));
        Assert.assertEquals(USER1_TASK1, service.findOneById(USER1.getId(), USER1_TASK1.getId()));
        thrown.expect(EntityNotFoundException.class);
        Assert.assertEquals(USER1_TASK2, service.findOneById(USER1_TASK2.getId()));
        Assert.assertEquals(USER2_TASK1, service.findOneById(USER1.getId(), USER2_TASK1.getId()));
    }

    @Test
    public void remove() {
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        Assert.assertEquals(4, service.getSize());
        service.remove(USER1_TASK1);
        Assert.assertEquals(3, service.getSize());
        service.removeById(USER1_TASK2.getId());
        Assert.assertEquals(2, service.getSize());
        Assert.assertEquals(USER1_TASK3, service.findAll().get(0));
        Assert.assertEquals(USER2_TASK1, service.findAll().get(1));
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void removeByUserId() {
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        Assert.assertEquals(4, service.getSize());
        service.remove(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(3, service.getSize());
        service.removeById(USER1.getId(), USER1_TASK2.getId());
        Assert.assertEquals(2, service.getSize());
        service.remove(USER2.getId(), USER1_TASK3);
        Assert.assertEquals(2, service.getSize());
        Assert.assertEquals(USER1_TASK3, service.findAll().get(0));
        Assert.assertEquals(USER2_TASK1, service.findAll().get(1));
    }

    @Test
    public void create() {
        service.create(USER2.getId(), "task-2", "description of task 2");
        Assert.assertEquals(1, service.getSize());
        Assert.assertEquals("task-2", service.findAll().get(0).getName());
        Assert.assertEquals("description of task 2", service.findAll().get(0).getDescription());
        Assert.assertEquals(Status.NOT_STARTED, service.findAll().get(0).getStatus());
    }

    @Test
    public void findAllByProjectId() {
        service.add(USER1_TASK_LIST);
        service.add(USER2_TASK_LIST);
        service.add(ADMIN1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST, service.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void updateById() {
        @NotNull final Task task = service.create(USER1.getId(),
                "task-4", "description of task 4");
        service.updateById(USER1.getId(), task.getId(),
                "upd-task-4", "upd description of task 4");
        Assert.assertEquals("upd-task-4", task.getName());
        Assert.assertEquals("upd description of task 4", task.getDescription());
    }

    @Test
    public void changeTaskStatusById() {
        service.add(USER1_TASK1);
        Assert.assertEquals(Status.NOT_STARTED, USER1_TASK1.getStatus());
        service.changeTaskStatusById(USER1.getId(), USER1_TASK1.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, USER1_TASK1.getStatus());
    }
    
}
