package ru.t1.ktitov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.repository.IUserRepository;
import ru.t1.ktitov.tm.marker.UnitCategory;

import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @After
    public void tearDown() {
        repository.clear();
    }

    @Test
    public void add() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findAll().get(0));
    }

    @Test
    public void addList() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER_LIST);
        Assert.assertEquals(3, repository.getSize());
        Assert.assertEquals(USER_LIST, repository.findAll());
        Assert.assertEquals(USER1, repository.findAll().get(0));
        Assert.assertEquals(USER2, repository.findAll().get(1));
        Assert.assertEquals(ADMIN1, repository.findAll().get(2));
    }

    @Test
    public void set() {
        repository.add(USER_LIST);
        repository.set(USER_LIST2);
        Assert.assertEquals(USER_LIST2, repository.findAll());
    }

    @Test
    public void clear() {
        repository.add(USER_LIST);
        Assert.assertEquals(3, repository.getSize());
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void findAll() {
        repository.add(USER_LIST);
        Assert.assertEquals(USER_LIST, repository.findAll());
        Assert.assertEquals(USER_LIST, repository.findAll());
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void existsById() {
        repository.add(USER_LIST);
        Assert.assertTrue(repository.existsById(USER1.getId()));
        Assert.assertTrue(repository.existsById(USER2.getId()));
        Assert.assertFalse(repository.existsById(USER3.getId()));
        Assert.assertTrue(repository.existsById(ADMIN1.getId()));
    }

    @Test
    public void findOneById() {
        repository.add(USER_LIST);
        Assert.assertEquals(USER1, repository.findOneById(USER1.getId()));
        Assert.assertEquals(USER2, repository.findOneById(USER2.getId()));
        Assert.assertFalse(repository.existsById(USER3.getId()));
        Assert.assertEquals(ADMIN1, repository.findOneById(ADMIN1.getId()));
    }

    @Test
    public void remove() {
        repository.add(USER_LIST);
        Assert.assertEquals(3, repository.getSize());
        repository.remove(USER1);
        Assert.assertEquals(2, repository.getSize());
        repository.remove(USER3);
        Assert.assertEquals(2, repository.getSize());
        repository.removeById(USER2.getId());
        Assert.assertEquals(1, repository.getSize());
        Assert.assertEquals(ADMIN1, repository.findAll().get(0));
    }

    @Test
    public void findByLogin() {
        repository.add(USER_LIST);
        Assert.assertEquals(USER1, repository.findByLogin("user0"));
        Assert.assertEquals(USER2, repository.findByLogin("user1"));
        Assert.assertEquals(ADMIN1, repository.findByLogin("user2"));
        Assert.assertNull(repository.findByLogin("admin1"));
    }

    @Test
    public void findByEmail() {
        repository.add(USER_LIST);
        Assert.assertEquals(USER1, repository.findByEmail("user0@gmail.com"));
        Assert.assertEquals(USER2, repository.findByEmail("user1@gmail.com"));
        Assert.assertEquals(ADMIN1, repository.findByEmail("user2@gmail.com"));
        Assert.assertNull(repository.findByEmail("admin1@gmail.com"));
    }

    @Test
    public void isLoginExist() {
        repository.add(USER_LIST);
        Assert.assertTrue(repository.isLoginExist("user0"));
        Assert.assertTrue(repository.isLoginExist("user1"));
        Assert.assertTrue(repository.isLoginExist("user2"));
        Assert.assertFalse(repository.isLoginExist("admin1"));
    }

    @Test
    public void isEmailExist() {
        repository.add(USER_LIST);
        Assert.assertTrue(repository.isEmailExist("user0@gmail.com"));
        Assert.assertTrue(repository.isEmailExist("user1@gmail.com"));
        Assert.assertTrue(repository.isEmailExist("user2@gmail.com"));
        Assert.assertFalse(repository.isEmailExist("admin1@gmail.com"));
    }

}
