package ru.t1.ktitov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.repository.ISessionRepository;
import ru.t1.ktitov.tm.marker.UnitCategory;

import static ru.t1.ktitov.tm.constant.SessionTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository repository = new SessionRepository();

    @After
    public void tearDown() {
        repository.clear();
    }

    @Test
    public void add() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, repository.findAll().get(0));
    }

    @Test
    public void addList() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION_LIST);
        Assert.assertEquals(3, repository.getSize());
        Assert.assertEquals(USER1_SESSION_LIST, repository.findAll());
        Assert.assertEquals(USER1_SESSION1, repository.findAll().get(0));
        Assert.assertEquals(USER1_SESSION2, repository.findAll().get(1));
        Assert.assertEquals(USER1_SESSION3, repository.findAll().get(2));
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1.getId(), USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, repository.findAll().get(0));
    }

    @Test
    public void clear() {
        repository.add(USER1.getId(), USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, repository.findAll().get(0));
        repository.clear();
        Assert.assertTrue(repository.findAll().isEmpty());
    }

    @Test
    public void clearByUserId() {
        repository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, repository.findAll());
        repository.clear(USER2.getId());
        Assert.assertFalse(repository.findAll().isEmpty());
        repository.clear(USER1.getId());
        Assert.assertTrue(repository.findAll().isEmpty());
        repository.add(USER1_SESSION1);
        repository.add(USER2_SESSION1);
        repository.clear(USER1.getId());
        Assert.assertEquals(USER2_SESSION1, repository.findAll().get(0));
    }

    @Test
    public void findAll() {
        repository.add(USER1_SESSION_LIST);
        repository.add(USER2_SESSION_LIST);
        Assert.assertEquals(6, repository.getSize());
        Assert.assertEquals(USER1_SESSION_LIST, repository.findAll(USER1.getId()));
        Assert.assertEquals(USER2_SESSION_LIST, repository.findAll(USER2.getId()));
    }

    @Test
    public void findOneById() {
        repository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION1, repository.findOneById(USER1_SESSION1.getId()));
        Assert.assertNull(repository.findOneById(USER2.getId(), USER1_SESSION1.getId()));
        Assert.assertEquals(USER1_SESSION1, repository.findOneById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertTrue(repository.existsById(USER1_SESSION1.getId()));
        Assert.assertFalse(repository.existsById(USER2.getId(), USER1_SESSION1.getId()));
        Assert.assertTrue(repository.existsById(USER1.getId(), USER1_SESSION1.getId()));
    }

    @Test
    public void remove() {
        repository.add(USER1_SESSION_LIST);
        repository.remove(USER1_SESSION1);
        Assert.assertEquals(2, repository.getSize());
        repository.removeById(USER1_SESSION2.getId());
        Assert.assertEquals(1, repository.getSize());
        Assert.assertEquals(USER1_SESSION3, repository.findAll().get(0));
        repository.clear();
        repository.add(USER1_SESSION_LIST);
        Assert.assertEquals(3, repository.getSize());
        repository.removeById(USER2.getId(), USER1_SESSION1.getId());
        Assert.assertEquals(3, repository.getSize());
        repository.removeById(USER1.getId(), USER1_SESSION1.getId());
        repository.removeById(USER1.getId(), USER1_SESSION2.getId());
        Assert.assertEquals(USER1_SESSION3, repository.findAll().get(0));
    }

}
